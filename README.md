# Conceptos del Curso Básico de Vue.js 2
#### Proyecto del curso

![Proyect](https://static.platzi.com/media/landing-projects/proyecto-vue-js-profesional.png)

## Platzi Exchange
[Platzi Exchange](https://gitlab.com/industrias-gomez/platzi-exchange)

Platzi Exchange es una plataforma que te servirá para visualizar todas las criptomonedas y sus valores en tiempo real utilizando las ventajas de Vue.js un framework para desarrollar sitios web reactivos y basados en componentes muy utilizado en el mundo del desarrollo web


## License
[MIT](https://choosealicense.com/licenses/mit/)
