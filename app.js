Vue.component('CoinDetail', {

  props: ['coin'],

  data () {
    return {
      showPrices: false,
      value: 0,
    }
  },

  methods: {
    toggleShowPrices () {
      this.showPrices = !this.showPrices

      this.$emit('change-color', this.showPrices ? 'FF96C8' : '3D3D3D')
    }
  },

  computed: 
    {title () {
      return `${this.coin.name} - ${this.coin.symbol}`
    },

    converterdValue(){
      if(!this.value){
        return 0
      }
      return this.value / this.coin.actualPrice
    }
  },

  created () {
    console.log('Created CoinDetail...')
  },

  mounted () {
    console.log('Mounted CoinDetail...')
  },

  template: `
    <div>
      <img
        v-on:mouseover="toggleShowPrices"
        v-on:mouseout="toggleShowPrices"
        v-bind:src="coin.img"
        v-bind:alt="coin.name"
      />
      <h1 v-bind:class="coin.changePercent > 0 ? 'green' : 'red'">
        {{ title }}
        <span v-if="coin.changePercent > 0">👍</span>
        <span v-else-if="coin.changePercent < 0">👍</span>
        <span v-else>🤞</span>

        <span v-show="coin.changePercent > 0">👍</span>
        <span v-show="coin.changePercent < 0">👍</span>
        <span v-show="coin.changePercent === 0">🤞</span>

        <span v-on:click="toggleShowPrices"
          >{{ showPrices ? '🤑' : '😟' }}</span
        >
      </h1>

      <input type="number" v-model="value" />
      <span>{{ converterdValue }}</span>

      <slot name="text"></slot>
      <slot name="link"></slot>

      <ul>
        <li v-for="(price, i) in coin.prices" v-bind:key="price">
          {{ i }} - {{ price }}
        </li>
      </ul>

      <ul v-show="showPrices">
        <li
          class="uppercase"
          v-bind:class="{orange: price.value === coin.actualPrice, red: price.value < coin.actualPrice, green: price.value > coin.actualPrice }"
          v-for="(price, i) in coin.pricesWithDays"
          v-bind:key="price.day"
        >
          {{ i }} - {{ price.day }} - {{ price.value }}
        </li>
      </ul>

    </div>
  `
})

new Vue({
  el: '#app',

  data () {
    return {
      btc: {
        name: 'Bitcoin',
        symbol: 'BTC',
        img: 'https://cryptologos.cc/logos/bitcoin-btc-logo.png',
        changePercent: -10,
        actualPrice: 8400,
        prices: [8400, 7900, 8200, 9000, 9400, 10000, 10200],
        pricesWithDays: [
          { day: 'Lunes', value: 8400 },
          { day: 'Martes', value: 7900 },
          { day: 'Miercoles', value: 8200 },
          { day: 'Jueves', value: 9000 },
          { day: 'Viernes', value: 9400 },
          { day: 'Sabado', value: 10000 },
          { day: 'Domingo', value: 10200 },
        ],
      },
      
      color: 'f4f4f4',

    }
  },

  created () {
    console.log('Created...')
  },

  mounted () {
    console.log('Mounted...')
  },

  methods: {
    updateColor (color) {
      //Este metodo nos permite revertir los colores
      this.color = color || this.color
        .split('')
        .reverse()
        .join('')
    }
  }
})